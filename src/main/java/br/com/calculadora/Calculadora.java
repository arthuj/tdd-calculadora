package br.com.calculadora;

public class Calculadora {

    public Calculadora() {
    }

    public  int soma(int primeiroNumero, int segundoNumero)
    {
        int resultado = primeiroNumero + segundoNumero;
        return  resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero + segundoNumero;
        return  resultado;
    }

    public int multiplica(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero*segundoNumero;
        return resultado;
    }

    public double multiplica(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero*segundoNumero;
        return resultado;
    }

    public int dividir(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero/segundoNumero;
        return resultado;
    }

    public double dividir(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero/segundoNumero;
        return resultado;
    }

}
