package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp()
    {
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros()
    {
        int resultado = calculadora.soma(1,2);

        Assertions.assertEquals(3,resultado);
    }
    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3,3.4);

        BigDecimal resultadofinal = new BigDecimal(resultado).setScale(2,RoundingMode.HALF_EVEN);

        Assertions.assertEquals(5.7,resultadofinal.doubleValue());
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros()
    {
        int resultado = calculadora.dividir(4,2);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosFlutuantes()
    {
        double resultado = calculadora.dividir(2.2,1.1);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosNegativos()
    {
        double resultado = calculadora.dividir(-2.2,-1.1);

        Assertions.assertEquals(2,resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros()
    {
        int resultado = calculadora.multiplica(4,2);

        Assertions.assertEquals(8,resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes()
    {
        double resultado = calculadora.multiplica(2.2,1.1);

        BigDecimal resultadofinal = new BigDecimal(resultado).setScale(2,RoundingMode.HALF_EVEN);

        Assertions.assertEquals(2.42,resultadofinal.doubleValue());
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos()
    {

        double resultado = calculadora.multiplica(-2.2,-1.1);

        BigDecimal resultadofinal = new BigDecimal(resultado).setScale(2,RoundingMode.HALF_EVEN);

        Assertions.assertEquals(2.42,resultadofinal.doubleValue());
    }
}
